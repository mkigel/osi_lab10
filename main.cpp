#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int print_Status(int status)
{
    if(WIFEXITED(status))
    {
        printf("exit() with status: %d\n", WEXITSTATUS(status));
        return 0;
    }
    if(WIFSIGNALED(status))
    {
        printf("end after signal with number: %d\n", WTERMSIG(status));
        return 0;
    }

    return -1;
}

int main(int argc, char * argv[])
{
    int status;

    if(argc < 2)
    {
        perror("You need to enter program name and arguments");
        return EXIT_FAILURE;
    }

    printf("%s\n", argv[1]);
    pid_t genProcces = fork();

    if(genProcces == -1)
    {
        perror("Can't create process");
        return EXIT_FAILURE;
    }

    if(genProcces == 0)
    {
        execvp(argv[1], &argv[1]);
        perror(argv[1]);
        return EXIT_FAILURE;
    }

    pid_t waitResult = wait(&status);
    if(waitResult == -1)
    {
        perror("Error in function wait");
        return EXIT_FAILURE;
    }

    return  print_Status(status);
}

