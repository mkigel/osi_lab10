

#include <signal.h>
#include <stdio.h>
#include <sys/types.h>

int main(int argc, char ** argv)
{
    if (argc == 1)
    {
        kill(getpid(),SIGTERM);
        return -1;
    }
    return argc - 1;
}

